# JSMinifier

### Deep minifier for Javascript
c++ implementation of javascript minifier, featuring:
+ redudancy removal
+ variable renaming
+ functional simplification
+ co-minification with HTML references

### Usage
Pass in the root directory of your project, as:

```
jsminifier -d 'YOUR ROOT DIRECTORY'
```

 Program will find all child files when a .js extension and interpret them as javascript files.