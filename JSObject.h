#pragma once
#include <string>
#include <vector>
class JSObject
{
public: 
	std::string name;
	std::vector<JSObject> children;
	static JSObject* parse(std::string s);
};
